const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

module.exports = {
    mode: "development",
    devtool: "eval-source-map",
    devServer: {
        contentBase: path.join(__dirname, "dist"),
        index: "index.html",
        port: 9000,
        compress: true,
        overlay: true,
    },
    entry: {
        main: "./src/js/index.js",
        view1: "./src/js/view1.js",
        view2: "./src/js/view2.js",
    },
    output: {
        filename: "js/[name].js",
        path: path.resolve(__dirname, "dist"),
    },
    module: {
        rules: [
            // Transpile JS files
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                loader: "babel-loader",
            },
            // Compile scss into css
            {
                test: /\.s[ac]ss$/i,
                use: [
                    "style-loader",
                    MiniCssExtractPlugin.loader,
                    {
                        loader: "css-loader",
                        options: {
                            // For importing inside of js file
                            modules: {
                                // keep original class names
                                localIdentName: "[local]",
                            },
                        },
                    },
                    "sass-loader",
                ],
            },
            // HTML files
            {
                test: /\.hbs$/,
                loader: "handlebars-loader",
            },
        ],
    },
    plugins: [
        // Clear dist folder before new build
        new CleanWebpackPlugin({ cleanStaleWebpackAssets: false }),
        // Generate HTML file from hbs template
        new HtmlWebpackPlugin({
            title: "Webpack Boilerplate",
            template: "./src/views/index.hbs",
            filename: "index.html",
            inject: true,
            cache: true,
        }),
        // Generate HTML file from hbs template, using only view1 files
        new HtmlWebpackPlugin({
            title: "Webpack Boilerplate",
            template: "./src/views/view1.hbs",
            filename: "view1.html",
            inject: true,
            cache: true,
            chunks: ["view1"],
        }),
        // Generate HTML file from hbs template, using only view2 files
        new HtmlWebpackPlugin({
            title: "Webpack Boilerplate",
            template: "./src/views/view2.hbs",
            filename: "view2.html",
            inject: true,
            cache: true,
            chunks: ["view2"],
        }),
        // Put styles into a separate file
        new MiniCssExtractPlugin({
            filename: "css/[name].css",
        }),
    ],
    resolve: {
        extensions: ["*", ".js", ".json"],
    },
};
