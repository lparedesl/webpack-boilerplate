class ModuleOne {
    constructor() {
        this._name = "module1.js";
    }

    get name() {
        return this._name;
    }
}

export default ModuleOne;
