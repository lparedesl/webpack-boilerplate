import "../scss/style.scss";
import ModuleOne from "./module1";

const fileName = "index.js";
const moduleOne = new ModuleOne();

console.log(`Hello from ${fileName}`);
console.log(`1st module's name: ${moduleOne.name}`);
