# Webpack boilerplate project

## Install dependencies

### Navigate into project directory
```
cd <directory>
```

### Install project dependencies
```
npm i -D webpack webpack-cli@^3.x.x webpack-dev-server clean-webpack-plugin
```

JS assets
```
npm i -D @babel/core @babel/preset-env babel-loader
```

CSS assets
```
npm i -D node-sass style-loader css-loader mini-css-extract-plugin sass-loader
```

HTML assets
```
npm i -D handlebars handlebars-loader html-webpack-plugin
```

### Create .babelrc configuration file

```json
{
    "presets": ["@babel/preset-env"]
}
```

## Create `webpack` configuration file

See `webpack.config.js`

## Run the webpack build

### Build js and css files

```
npm run build
```

### Build js and css files and live reload on file changes

```
npm run develop
```
